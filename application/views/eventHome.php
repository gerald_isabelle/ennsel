 <div class="container currentEvent">
	      	<div class="leftCurrent">
	      		<div class="currentContent">
	      			<div class="currentDate"><span class="date">16</span><span class="yearMonth">Mar 2013</span></div>
	      			<div class="currentImage"><a href="<?php echo(base_url()); ?>events" title="Ras Nininn Live in London"><img src="img/rassNinninLive.jpg" alt="Ras Nininn live in london"></a></div>
	      			<div class="currentText">
	      				<h2><a href="<?php echo(base_url()); ?>events" title="best of Mauritian live music">Ras Nininn 1st time Live in London</a></h2>
	      				<h3>El Peñol Club - South London's Finest</h3>
	      				<div class="ticket"><span>Ticket: </span>£15 - <span><a href="http://ennsel.eventbrite.co.uk/" target="_blank" title="buy tickets" >Buy Now</a></span></div>
<p>Celebrate the 45th Independence of Mauritius with Ras Nininn, a popular and up-and-coming seggae  and reggae artist from the island. <a href="<?php echo(base_url()); ?>events" title="more about the event">..more</a></p>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="rightCurrent">
	      		<h3>Forthcoming event:</h3>
	      		<!-- TIMER -->
					<div class="timer-area">
						
						
						<ul id="countdown">
							<li>
								<span class="days">00</span>
								<p class="timeRefDays">days</p>
							</li>
							<li>
								<span class="hours">00</span>
								<p class="timeRefHours">hours</p>
							</li>
							<li>
								<span class="minutes">00</span>
								<p class="timeRefMinutes">minutes</p>
							</li>
							<li>
								<span class="seconds">00</span>
								<p class="timeRefSeconds">seconds</p>
							</li>
						</ul>
						
					</div> <!-- end timer-area -->
	      		
	      	</div>
	      	
	      </div>
