<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        
        <?
			// populating meta data
			foreach ($content as $r) {

				echo '
				<title>'.ucfirst($r->name).' | Ennsel Promotions</title>
		        <meta name="description" content="'.$r->description.'">
		        <meta name="keywords" content="'.$r->keywords.'">
		        <meta property="og:site_name" content="Ennsel Promotions" />
		        <meta property="og:title" content="'.ucfirst($r->name).' | Ennsel Promotions" />
				<meta property="og:type" content="website" />
				<meta property="og:image" content='.base_url().'img/logo.jpg />
				<meta property="og:description" content="'.$r->description.'" />
				<meta itemprop="description" content="'.$r->description.'">';
			}
		?>
        
		
		
		
       <!--Disable resizing <meta name="viewport" content="width=device-width"> -->
		
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo(base_url()); ?>css/bootstrap.css">
        <script src="<?php echo(base_url()); ?>js/vendor/modernizr-2.6.2.min.js"></script>
        
        
    </head>