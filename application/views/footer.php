 <div class="container">
	      <div class="row social">
	        <div class="span6">
	          <div class="well facebook">
	            <div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like-box" data-href="https://www.facebook.com/ennselpromotionsUK" data-width="457" data-height="350" data-show-faces="true" data-border-color="white" data-stream="false" data-header="false"></div>
	          </div>
	        </div>
	        <div class="span6">
	          <div class="well twitter">
	            <a class="twitter-timeline" href="https://twitter.com/EnnSelPromotion" data-widget-id="281157370229424128">Tweets by @EnnSelPromotion</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	          </div>
	        </div>
	      </div>
	      <div class="container sponsors"><h4>Our Sponsors</h4>
	      	<a href="http://www.mauritiancuisine.co.uk/" title="mauritian cuisine" target="_blank"><img src="img/sponsors/mauritiancuisine.jpg" alt="mauritian cuisine" /></a>
	     	<a href="https://www.facebook.com/mangedesiles.mangedesiles/" title="manges des iles" target="_blank"><img src="img/sponsors/mangesdesiles.jpg" alt="manges des iles" /></a>
	     	<img src="img/sponsors/voiceofmauritius.jpg" alt="voice of mauritius" />
	     	<a href="http://www.radiomoris.com" title="radio moris" target="_blank"><img src="img/sponsors/radiomoriss1.jpg" alt="radio moris" /></a>
	      </div>
	    </div>
		</div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="<?php echo(base_url()); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo(base_url()); ?>js/plugins.js"></script>
        <script src="<?php echo(base_url()); ?>js/main.js"></script>

         <script>
	
		
	
	</script>
	
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-37354164-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
