    <body>
    	 <div class="container headerSocial">
    	 	<div class="socialButtons">
	    	 	<a href="https://twitter.com/EnnSelPromotion/" title="follow us on twitter" target="_blank"><img src="<?php echo(base_url()); ?>img/twitter.png" alt="twitter"  /></a>
	    	 	<a href="https://www.facebook.com/ennselpromotionsUK" title="follow us on facebook" target="_blank"><img src="<?php echo(base_url()); ?>img/facebook.png" alt="facebook" /></a>
	    	</div>
    	 </div>
    	 <div class="container"><div class="logo"><a href="<?php echo(base_url()); ?>" title="Ennsel Promotions"><img src="<?php echo(base_url()); ?>img/logo.png" class="responsive-logo" alt="Ennsel logo" /></a></div>
	     <div class="navbar">
	      <div class="navbar-inner">
	          <div class="navbar-content">
	            <ul class="nav  pull-right">
	            
	            <?
			$classContact=$classhome=$events=$aboutus=$news=$gallery=$buyTickets='';
			$class = 'class="active"';
			
			switch ($this->uri->segment(1)) {
				case 'contact':
					$classContact = $class;						
					break;
				case 'events':
					$events = $class;						
					break;
				case 'about':
					$aboutus = $class;						
					break;
				case 'gallery':
					$gallery = $class;						
					break;
				case 'tickets':
					$buyTickets = $class;						
					break;
				case 'news':
					$news = $class;						
					break;
				default:
					$classhome = $class;
					break;
			}
			
			
				echo '
			<li '.$classhome.'>'.anchor('/','home','title="home"').'</li>
			<li '.$events.'>'.anchor('/events','events','title="events"').'</li>
			<li '.$aboutus.'>'.anchor('/about','about us','title="about us"').'</li>
			<li '.$classContact.'>'.anchor('/contact','contact us','title="contact us"').'</li>
			
				';
			?>	
	            	<!--elements of nav which needs to be added in the future
	            		<li '.$news.'>'.anchor('/news','news','title="news"').'</li>
	            		<li '.$gallery.'>'.anchor('/gallery','gallery','title="gallery"').'</li>
						<li '.$buyTickets.'>'.anchor('/tickets','buy tickets','title="buy tickets"').'</li>-->
	            	
	             

	              </li>
	            </ul>
	            
	            
	            
	          </div>
	      </div>
	    </div>
	   </div>