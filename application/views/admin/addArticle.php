<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
	<script src="<?php echo(base_url()); ?>js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
	<script type="text/javascript">
        tinyMCE.init({
            theme : "advanced",
            mode : "exact",
            elements : "addTinymce",
            plugins : "imagemanager,filemanager,insertdatetime,preview,emotions,visualchars,nonbreaking",
            theme_advanced_buttons1_add: 'insertimage,insertfile',
            theme_advanced_buttons2_add: 'separator,forecolor,backcolor',
            theme_advanced_buttons3_add: 'emotions,insertdate,inserttime,preview,visualchars,nonbreaking',
            theme_advanced_disable: "styleselect,formatselect,removeformat",
            plugin_insertdate_dateFormat : "%Y-%m-%d",
            plugin_insertdate_timeFormat : "%H:%M:%S",
            theme_advanced_toolbar_location : "top",
        	theme_advanced_toolbar_align : "left",
       		theme_advanced_statusbar_location : "bottom",
            theme_advanced_resize_horizontal : false,
            theme_advanced_resizing : true,
            apply_source_formatting : true,
            spellchecker_languages : "+English=en",
            extended_valid_elements :"img[src|border=0|alt|title|width|height|align|name],"
            +"a[href|target|name|title],"
            +"p,"
         //   invalid_elements: "table,span,tr,td,tbody,font"

        });  
     </script> 
	<title></title>
</head>
<body>


	<?
		
		
		$articleTextarea = array('name'=>'article','id'=>'addTinymce','rows'=>5, 'cols'=>'40');
		//$Fdata = array('name' => 'imageUpload', 'class' => 'file'); // set your file and class for the image*/
		
		echo '
			
			'.validation_errors('<div class="error">', '</div>').'
			
			'.form_open_multipart('backend/addArticle').'
			'.form_label('Enter Article Name', 'articleName').' '.form_input('articleName').'<br/>
			'.form_label('Article', 'article').' '.form_textarea($articleTextarea).'<br/>
			'.form_label('Description', 'description').' '.form_textarea('description').'<br/>
			'.form_label('Tags (keywords)', 'tag').' '.form_input('tag').'<br/>	
			'.form_submit('mysubmit', 'Submit Article').'
			
			
			'.form_close().'
			
			
		';
		
	?>

</body>
</html>