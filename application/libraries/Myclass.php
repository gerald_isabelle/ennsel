<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Myclass {
	
	// create slugs
    function createSlug($s, $separator = 'dash', $chars_to_keep = '/[^a-z0-9_-]/' ){
		    // This way we don't have to mess with uppercase the rest of the time
		    $s = rtrim(strtolower(htmlentities(strip_tags(str_replace('?','',utf8_decode($s))))));
		    
		    // We don't need to capture the second group, so I made it optional || added slash for oslash
		    $s = preg_replace ('/&([a-z])(?:uml|acute|grave|circ|tilde|cedil|ring|slash);/', '$1', $s);
		    // remove unwanted chars
		    // Weird characters that don't get caught above - also includes &eth;and &thorn;, but I don't know what the best replacement for those would be.
		    // While we're at it, we'll also get the http
		    $s = str_replace( array('&szlig;', '&aelig;', '&oelig;', 'http://'), array('ss', 'ae', 'oe', ''), $s);
		    
		    $s = html_entity_decode($s);
		    
		    // Normalize multiple spaces, dashes, and underscores
		    $s = preg_replace( array('/\&/', '/\s+/', '/-+/', '/_+/'), ($separator=='dash')?'-':'_', $s);
		
		    // Remove unwanted chars
		    $s = preg_replace($chars_to_keep, '', $s);
		    
		    return $s;
	} 


}