<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ennsel extends CI_Controller {

	
	public function __construct(){
		parent::__construct();
		
		// main model
		$this->load->model('front_end');
	}
	
	
	
	public function index()
	{
		$data['content'] = $this->front_end->getPageContent('home');
		$this->load->view('header',$data);
		$this->load->view('navigation');
		$this->load->view('carousel');
		$this->load->view('introHome');
	//$this->load->view('eventHome');
		$this->load->view('footer');
		
	}
	
	public function events()
	{
		$data['content'] = $this->front_end->getPageContent($this->uri->segment(1));
		$this->load->view('header',$data);
		$this->load->view('navigation');
		$this->load->view('eventNew',$data);
		$this->load->view('footer');
		
	}
	
	public function gallery()
	{
		$data['content'] = $this->front_end->getPageContent($this->uri->segment(1));
		$this->load->view('header',$data);
		$this->load->view('navigation');

		$this->load->view('footer');
		
	}
	
	public function news()
	{
		if($this->uri->segment(2)){
			$data['content'] = $this->front_end->getSingleNews($this->uri->segment(2));
			$this->load->view('header',$data);
			$this->load->view('navigation');
			$this->load->view('pageBody',$data);
			$this->load->view('footer');
		}else{
			$data['content'] = $this->front_end->getPageContent($this->uri->segment(1));
			$data['news'] = $this->front_end->listAllNews();
			$this->load->view('header',$data);
			$this->load->view('navigation');
			$this->load->view('newsBody',$data);
			$this->load->view('footer');
		}
		
	}
	
	public function about()
	{
		$data['content'] = $this->front_end->getPageContent($this->uri->segment(1));
		$this->load->view('header',$data);
		$this->load->view('navigation');
		$this->load->view('pageBody',$data);
		$this->load->view('footer');
		
	}
	
	public function tickets()
	{
		$this->load->view('header');
		$this->load->view('navigation');

		$this->load->view('footer');
		
	}
	
	public function contact()
	{
		$data['content'] = $this->front_end->getPageContent($this->uri->segment(1));
		$this->load->view('header', $data);
		$this->load->view('navigation');
		$this->load->view('pageBody',$data);
		$this->load->view('footer');
		
	}





}

