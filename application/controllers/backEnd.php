<?
class BackEnd extends CI_Controller {
	
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('back_end');
		$this->load->library('form_validation');
			
	}
	
	public function index()
	{
		$this->load->view('admin/addArticle');
	}
	
	//insert new article in the database
	public function addArticle(){
	
		
		$data = $_POST;		
		if($data){
			$this->back_end->addArticles($data);
		}	
		
			
		$this->load->view('admin/addArticle');
		
	}
	
}
?>